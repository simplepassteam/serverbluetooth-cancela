String message; //string that stores the incoming message
#include <Stepper.h> // Incluir Biblioteca 'Stepper'

const int passosPorVolta = 500;
Stepper myStepper(passosPorVolta, A1, A2, A3, A4); 
int passos = 500; // Número de passos dados
String parar_app = "stop_ble";

void setup()
{
  Serial.begin(9600); //set baud rate
  myStepper.setSpeed(60); // velocidade do motor em 60;
}

void loop()
{
  while(Serial.available())
  {//while there is data available on the serial monitor
    message+=char(Serial.read());//store string from serial command
  }
  if(!Serial.available())
  {
    if(message == "open")
    {//if data is available
      Serial.print(message);
      message=""; //clear the data      

      
      myStepper.step(passos); //Abre o portão!
      delay(2000); //delay
      myStepper.step(-passos); //fecha o portão!
      
    }
  }
}
